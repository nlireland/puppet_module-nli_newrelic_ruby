class newrelic_ruby::params {
  $install_path          = undef
  $agent_enabled         = false
  $log_level             = 'info'
  $app_name              = undef
  $newrelic_license_key  = undef
  $user                  = 'newrelic'
  $group                 = 'newrelic'
}
