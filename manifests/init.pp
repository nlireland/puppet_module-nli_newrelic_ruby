 class newrelic_ruby (
  $install_path          = $newrelic_ruby::params::install_path,
  $agent_enabled         = $newrelic_ruby::params::agent_enabled,
  $log_level             = $newrelic_ruby::params::log_level,
  $app_name              = $newrelic_ruby::params::app_name,
  $newrelic_license_key  = $newrelic_ruby::params::newrelic_license_key,
  $user                  = $newrelic_ruby::params::user,
  $group                 = $newrelic_ruby::params::group,
) inherits newrelic_ruby::params {

  if $newrelic_license_key == undef {
    fail('The newrelic_license_key parameter must be defined.')
  }

  if $install_path == undef {
    fail('Please specify an install path for the newrelic ruby agent config file. This is usually the config folder of your rails app.')
  }

  if $app_name == undef {
    fail('Please specify the app_name that new relic will used to collate reports.')
  }

  file { 'newrelic.yml':
    ensure  => present,
    owner   => $user,
    group   => $group,
    path    => "$install_path/newrelic.yml",
    content => template('newrelic_ruby/newrelic_yml.erb'),
  }
}
