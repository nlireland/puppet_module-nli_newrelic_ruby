# README #

A puppet module to install the new relic ruby agent configuration file.

## Limitations ##
* This module assumes you have installed the newrelic ruby agent gem, i.e. via your application Gemfile
* This module only allows the basic params to be overwritten. Adding more configuration elements, would require adding more module params or re-implementing a more dynamic way to generate the config file, e.g. augeas. A complete list of params is available here: https://docs.newrelic.com/docs/agents/ruby-agent/installation-configuration/ruby-agent-configuration

## Usage ##

**Note, you must set agent_enabled to true as the default is false to avoid unintentional billing. Also make sure you add a require param to the declaration to ensure this module is loaded after the main rails app has been installed. Also make sure you add a notify param to the declaration to ensure the rails environment is reloaded.**

```
#!puppet

node 'mynode' {
  class { 'newrelic_ruby':
    agent_enabled        => 'true', # optional, defaults to 'false'
    newrelic_license_key => 'YOUR_LICENSE_KEY', # required
    install_path         => 'path to rails app config folder or shared capistano config folder', # required
    app_name             => 'app_name that new relic will used to collate reports', # required
    user                 => 'YOUR_APP_USER', # optional, defaults to 'newrelic'
    group                => 'YOUR_APP_GROUP', # optional, defaults to 'newrelic'
    log_level            => 'info', # optional, defaults to 'info'
    require              => Class['your rails app puppet module'], # need to make sure your rails app is already installed
    notify               => Service['httpd'], # need to restart the web server to load new config
  }
}
```

Or, if your params are set in Hiera:


```
#!puppet

node 'mynode' {
  include newrelic_ruby
}
```

